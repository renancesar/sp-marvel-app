import React from 'react';
import {render, cleanup, makeTestStore, waitFor} from '../../test-utils';
import CharacterDetails from './CharacterDetails';
import {findOne, setCurrent} from "../../store/ducks/CharacterDucks";
import {add, fetchSeries, nextPage, setCurrentChatId, setHasMore, setLoading} from "../../store/ducks/SeriesDucks";
import {fireEvent} from "@testing-library/react";

afterEach(cleanup)

describe('CharacterDetails Spec', () => {
    const char = {
        id: 1,
        name: 'Joao',
        thumbnail: {
            path: 'http://route-to-char-picture.com/1',
            extension: 'jpg'
        }
    }

    const series = {total: 1, items: [{
        id: 100,
        title: 'The 100',
        description: 'The 100 series description'
    }]}

    it('should render and compare snapshot', () => {
        const {asFragment} = render(<CharacterDetails match={{params: {id: 1}}} />)
        expect(asFragment()).toMatchSnapshot()
    })

    it('should dispatch findOne when render component with param id', () => {
        const store = makeTestStore()
        render(<CharacterDetails match={{params: {id: 1}}} />, {store})
        expect(store.dispatch).toHaveBeenCalledWith(findOne(1))
    })

    it('should render component with char data', () => {
        const store = makeTestStore()
        const {asFragment, getByText, getByTestId} = render(<CharacterDetails match={{params: {id: 1}}} />, {store})
        const charAvatarElement = getByTestId('character-detail-char-avatar')
        expect(asFragment()).toMatchSnapshot()
        store.dispatch(setCurrent(char))

        expect(asFragment()).toThrowErrorMatchingSnapshot()
        expect(getByText('Joao')).toBeInTheDocument()
        expect(charAvatarElement.src).toBe('http://route-to-char-picture.com/1.jpg')
    })

    it('should call distach actions to load series when has char', async () => {
        const store = makeTestStore()
        store.dispatch(setCurrent(char))
        render(<CharacterDetails match={{params: {id: 1}}} />, {store})
        expect(store.dispatch).toHaveBeenCalledWith(setCurrentChatId(1))
        expect(store.dispatch).toHaveBeenCalledWith(fetchSeries())
    })

    it('should render serie item when has series', () => {
        const store = makeTestStore()
        store.dispatch(setCurrent(char))
        store.dispatch(add(series))
        const {getByText} =render(<CharacterDetails match={{params: {id: 1}}} />, {store})
        const serieItemTitleElement = getByText('The 100')
        expect(serieItemTitleElement).toBeInTheDocument()
    })

    it('should show Loader when is in loading', () => {
        const store = makeTestStore()
        store.dispatch(setCurrent(char))
        store.dispatch(add(series))
        const {getByTestId} =render(<CharacterDetails match={{params: {id: 1}}} />, {store})

        store.dispatch(setLoading(true))
        expect(getByTestId('app-loader')).toBeInTheDocument()
    })

    it('should hide Loader when is in loading', () => {
        const store = makeTestStore()
        store.dispatch(setCurrent(char))
        store.dispatch(add(series))
        const {queryByTestId} =render(<CharacterDetails match={{params: {id: 1}}} />, {store})

        store.dispatch(setLoading(false))
        expect(queryByTestId('app-loader')).toBeNull()
    })

    it('should hide View More when is in loading', () => {
        const store = makeTestStore()
        store.dispatch(setCurrent(char))
        store.dispatch(add(series))
        const {queryByText} =render(<CharacterDetails match={{params: {id: 1}}} />, {store})

        store.dispatch(setLoading(true))
        expect(queryByText('View More')).toBeNull()
    })

    it('should hide View More when is no has more', () => {
        const store = makeTestStore()
        store.dispatch(setCurrent(char))
        store.dispatch(add(series))
        const {queryByText} =render(<CharacterDetails match={{params: {id: 1}}} />, {store})
        store.dispatch(setLoading(false))
        store.dispatch(setHasMore(false))
        expect(queryByText('View More')).toBeNull()
    })

    it('should show View More when has more item and not is in loading', () => {
        const store = makeTestStore()
        store.dispatch(setCurrent(char))
        store.dispatch(add(series))
        const {queryByText} =render(<CharacterDetails match={{params: {id: 1}}} />, {store})
        store.dispatch(setLoading(false))
        store.dispatch(setHasMore(true))
        expect(queryByText('View More')).toBeInTheDocument()
    })

    it('should call dispatch with nextPage and fetchSeries when view more clicked', () => {
        const store = makeTestStore()
        store.dispatch(setCurrent(char))
        store.dispatch(add(series))
        const {getByText} =render(<CharacterDetails match={{params: {id: 1}}} />, {store})
        store.dispatch(setLoading(false))
        store.dispatch(setHasMore(true))
        const viewMoreElement = getByText('View More')
        expect(viewMoreElement).toBeInTheDocument()

        fireEvent.click(viewMoreElement)

        expect(store.dispatch).toHaveBeenCalledWith(nextPage())
        expect(store.dispatch).toHaveBeenCalledWith(fetchSeries())
    })

    it('should show editable mode when Edit Profile button clicked', () => {
        const store = makeTestStore()
        store.dispatch(setCurrent(char))
        const {getByText, getByTestId, queryByText} =render(<CharacterDetails match={{params: {id: 1}}} />, {store})
        const editProfileButtonElement = getByText('Edit profile')

        const nameLabelProfileReadOnly = getByText('Joao')

        expect(nameLabelProfileReadOnly).toBeInTheDocument()
        fireEvent.click(editProfileButtonElement)

        const nameInputProfileEditableElement = getByTestId('char-profile-editable-name-input')
        expect(nameInputProfileEditableElement).toBeInTheDocument()
        expect(queryByText('Joao')).toBeNull()
    })
})

