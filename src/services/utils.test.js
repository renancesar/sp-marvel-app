import {setItems, defineHasMore, mountPagination, mountFilterParams} from './utils'

describe('Utils Service Spec', () => {
    describe('.setItems', () => {
        it('should create map items by id', () => {
            const result = setItems({}, [
                {id: 1, name: 'test', description: 'desc', updateAt: '25/05/2020'}
            ])
            expect(result).toStrictEqual({'item-1': {id: 1, name: 'test', description: 'desc', updateAt: '25/05/2020'}})
        })

        it('should concat map with new items by id', () => {
            const result = setItems({
                'item-1': {
                    id: 1,
                    name: 'test',
                    description: 'desc',
                    updateAt: '25/05/2020'
                }
            }, [
                {id: 2, name: 'test 2', description: 'desc 2', updateAt: '26/05/2020'}
            ])
            expect(result).toStrictEqual({
                'item-1': {id: 1, name: 'test', description: 'desc', updateAt: '25/05/2020'},
                'item-2': {id: 2, name: 'test 2', description: 'desc 2', updateAt: '26/05/2020'}
            })
        })

        it('should update existent item in map', () => {
            const result = setItems({
                'item-1': {
                    id: 1,
                    name: 'test',
                    description: 'desc',
                    updateAt: '25/05/2020'
                }
            }, [
                {id: 1, name: 'test new name', description: 'desc new desc', updateAt: '26/05/2020'}
            ])
            expect(result).toStrictEqual({
                'item-1': {id: 1, name: 'test new name', description: 'desc new desc', updateAt: '26/05/2020'}
            })
        })

        it('should update only not omitted fields existent item in map', () => {
            const result = setItems({
                'item-1': {
                    id: 1,
                    name: 'test',
                    description: 'desc',
                    updateAt: '25/05/2020'
                }
            }, [
                {id: 1, name: 'test new name', description: 'desc new desc', updateAt: '26/05/2020'}
            ], ['name'])
            expect(result).toStrictEqual({
                'item-1': {id: 1, name: 'test', description: 'desc new desc', updateAt: '26/05/2020'}
            })
        })
    })

    describe('.defineHasMore', () => {
        it('should return true when total is bigger than pagination offset', () => {
            const pagination = {page: 10, limit: 10}
            const total = 120
            const result = defineHasMore(pagination, total)
            expect(result).toBe(true)
        })

        it('should return false when total is less than pagination offset', () => {
            const pagination = {page: 10, limit: 10}
            const total = 90
            const result = defineHasMore(pagination, total)
            expect(result).toBe(false)
        })

        it('should return false when total is equal than pagination offset', () => {
            const pagination = {page: 10, limit: 10}
            const total = 100
            const result = defineHasMore(pagination, total)
            expect(result).toBe(false)
        })
    })

    describe('.mountPagination', () => {
        it('should mount pagination object with pagination params', () => {
            const pagination = {page: 10, limit: 10}
            const result = mountPagination(pagination)

            expect(result).toStrictEqual({offset: 90, limit: 10})
        })
    })

    describe('.mountFilterParams', () => {
        it('should mount filter when has term', () => {
            const filter = {term: 'hero'}
            const result = mountFilterParams(filter)

            expect(result).toStrictEqual({nameStartsWith: 'hero'})
        })

        it('should dont mount filter when no has term', () => {
            const filter = {}
            const result = mountFilterParams(filter)

            expect(result).toEqual(undefined)
        })
    })
})