import CharacterReducer, {
    addCharacters,
    setLoading,
    nextPage,
    setError,
    setHasMore,
    setTotal,
    setCurrent,
    findOne,
    reset,
    setFilter,
    updateCharacterProfile,
    fetchCharacters,
    updateChar,
    Types
} from "./CharacterDucks";

describe('Character Ducks Spec', () => {
    describe('Actions', () => {
        it('should return correct action on addCharacters', () => {
            const payload = {total: 1, items: [{id: 1, name: 'Hero Name'}]}
            const action = addCharacters(payload)
            expect(action).toStrictEqual({
                type: Types.ADD,
                payload
            })
        })

        it('should return correct action on fetchCharacters', () => {
            const action = fetchCharacters()
            expect(action).toStrictEqual({
                type: Types.FETCH_ALL
            })
        })

        it('should return correct action on setLoading', () => {
            const payload = true
            const action = setLoading(payload)
            expect(action).toStrictEqual({
                type: Types.SET_LOADING,
                payload
            })
        })


        it('should return correct action on setTotal', () => {
            const payload = 1
            const action = setTotal(payload)
            expect(action).toStrictEqual({
                type: Types.SET_TOTAL,
                total: payload
            })
        })

        it('should return correct action on setHasMore', () => {
            const payload = true
            const action = setHasMore(payload)
            expect(action).toStrictEqual({
                type: Types.SET_HAS_MORE,
                hasMore: payload
            })
        })

        it('should return correct action on setError', () => {
            const payload = new Error('Error description')
            const action = setError(payload)
            expect(action).toStrictEqual({
                type: Types.SET_ERROR,
                payload: 'Error description'
            })
        })

        it('should return correct action on nextPage', () => {
            const action = nextPage()
            expect(action).toStrictEqual({
                type: Types.NEXT_PAGE
            })
        })

        it('should return correct action on setCurrent', () => {
            const payload = {id: 1, name: 'Hero Name'}
            const action = setCurrent(payload)
            expect(action).toStrictEqual({
                type: Types.SET_CURRENT,
                payload
            })
        })

        it('should return correct action on findOne', () => {
            const payload = 1
            const action = findOne(payload)
            expect(action).toStrictEqual({
                type: Types.FIND_ONE,
                id: payload
            })
        })

        it('should return correct action on reset', () => {
            const action = reset()
            expect(action).toStrictEqual({
                type: Types.RESET
            })
        })

        it('should return correct action on reset', () => {
            const payload = {term: 'Hero'}
            const action = setFilter(payload)
            expect(action).toStrictEqual({
                type: Types.SET_FILTER,
                payload
            })
        })

        it('should return correct action on setFilter', () => {
            const payload = {term: 'Hero'}
            const action = setFilter(payload)
            expect(action).toStrictEqual({
                type: Types.SET_FILTER,
                payload
            })
        })

        it('should return correct action on updateChar', () => {
            const payload = {name: 'Hero new name'}
            const id = 1
            const action = updateChar(id, payload)
            expect(action).toStrictEqual({
                type: Types.UPDATE_CHAR,
                payload,
                id
            })
        })

        it('should return correct action on updateCharacterProfile', () => {
            const payload = {name: 'Hero new name'}
            const id = 1
            const action = updateCharacterProfile(id, payload)
            expect(action).toStrictEqual({
                type: Types.UPDATE_CHAR_PROFILE,
                payload,
                id
            })
        })
    })

    describe('Reducer', () => {
        it('should return new state on action setLoading dispatched', () => {
            const payload = false
            const action = setLoading(payload)
            const result = CharacterReducer({}, action)
            expect(result.loading).toBe(false)
        })

        it('should return new state on action setTotal dispatched', () => {
            const payload = 1
            const action = setTotal(payload)
            const result = CharacterReducer({}, action)
            expect(result.total).toBe(1)
        })

        it('should return new state on action setHasMore dispatched', () => {
            const payload = false
            const action = setHasMore(payload)
            const result = CharacterReducer({}, action)
            expect(result.hasMore).toBe(false)
        })

        it('should return new state on action setError dispatched', () => {
            const payload = new Error('Error description')
            const action = setError(payload)
            const result = CharacterReducer({}, action)
            expect(result.error).toBe('Error description')
        })

        it('should return new state on action nextPage dispatched', () => {
            const action = nextPage()
            const result = CharacterReducer({pagination: {page: 1}}, action)
            expect(result.pagination.page).toBe(2)
        })

        it('should return new state on action addCharacters dispatched', () => {
            const payload = {total: 1, items: [{id: 1, name: 'Hero Name'}]}
            const action = addCharacters(payload)
            const result = CharacterReducer({}, action)
            expect(result.characters).toStrictEqual({
                'item-1': {id: 1, name: 'Hero Name'}
            })
        })

        it('should return new state on action setCurrent dispatched', () => {
            const payload = {id: 1, name: 'Hero Name'}
            const action = setCurrent(payload)
            const result = CharacterReducer({}, action)
            expect(result.current).toStrictEqual({id: 1, name: 'Hero Name'})
        })

        it('should return new state on action reset dispatched', () => {
            const action = reset()
            const result = CharacterReducer({}, action)
            expect(result.current).toStrictEqual({})
            expect(result.characters).toStrictEqual({})
            expect(result.total).toBe(0)
            expect(result.pagination).toStrictEqual({page: 1, limit: 20})
        })

        it('should return new state on action setFilter dispatched', () => {
            const action = setFilter({term: 'hero'})
            const result = CharacterReducer({filter: {}}, action)
            expect(result.filter.term).toBe('hero')
        })

        it('should return new state on action updateChar dispatched', () => {
            const id = 1
            const payload = {id: 1, name: 'new name'}
            const action = updateChar(id, payload)
            const result = CharacterReducer({characters: {
                'item-1': {id: 1, name: 'Hero Name'}
            }}, action)

            expect(result.characters).toStrictEqual({
                'item-1': {
                    id: 1,
                    name: 'new name'
                }
            })
        })
    })
})