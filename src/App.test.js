import React from 'react';
import { render, cleanup } from './test-utils'
import App from './App';

afterEach(cleanup)

test('renders learn react link', () => {
  const { getByTestId } = render(<App />);
  const mainContainer = getByTestId('app-main-container');
  expect(mainContainer).toBeInTheDocument();
});
