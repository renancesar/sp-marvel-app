import React, {useState} from 'react';
import './style.scss'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import {useDispatch} from "react-redux";
import {setFilter, fetchCharacters, reset} from "../../../store/ducks/CharacterDucks";

function SearchBarComponent(props) {
    const dispatch  = useDispatch()
    const [term, setTerm] = useState('')
    const searchChars = (e) => {
        e.preventDefault();
        dispatch(reset())
        dispatch(setFilter({term}))
        dispatch(fetchCharacters())
    }

    return (
        <>
            <form onSubmit={searchChars} className="search-form d-flex">
                <FontAwesomeIcon icon={faSearch} className="searchIcon" size="lg"/>
                <input type="text" value={term} onChange={(e)=> setTerm(e.target.value)} className="input-search" placeholder="Search here for the character you want"/>
                <button type="submit" className="btn btn-marvel ml-3 rounded-pill">Search!</button>
            </form>
        </>
    );
}

export default SearchBarComponent;