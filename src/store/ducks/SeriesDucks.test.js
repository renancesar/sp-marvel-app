import SeriesReducer, {
    add, fetchSeries, setLoading, setTotal, setHasMore, setError, nextPage, setCurrentChatId, Types
} from "./SeriesDucks";
describe('Series Ducks Spec', () => {
    describe('.Actions', () => {
        it('should return correct action on add', () => {
            const payload = {total: 1, items: [{id: 1, title: 'Serie Title'}]}
            const action = add(payload)
            expect(action).toStrictEqual({
                type: Types.ADD,
                payload
            })
        })

        it('should return correct action on fetchSeries', () => {
            const action = fetchSeries()
            expect(action).toStrictEqual({
                type: Types.FETCH_ALL
            })
        })

        it('should return correct action on setLoading', () => {
            const payload = true
            const action = setLoading(payload)
            expect(action).toStrictEqual({
                type: Types.SET_LOADING,
                payload
            })
        })


        it('should return correct action on setTotal', () => {
            const payload = 1
            const action = setTotal(payload)
            expect(action).toStrictEqual({
                type: Types.SET_TOTAL,
                total: payload
            })
        })

        it('should return correct action on setHasMore', () => {
            const payload = true
            const action = setHasMore(payload)
            expect(action).toStrictEqual({
                type: Types.SET_HAS_MORE,
                hasMore: payload
            })
        })

        it('should return correct action on setError', () => {
            const payload = new Error('Error description')
            const action = setError(payload)
            expect(action).toStrictEqual({
                type: Types.SET_ERROR,
                payload: 'Error description'
            })
        })

        it('should return correct action on nextPage', () => {
            const action = nextPage()
            expect(action).toStrictEqual({
                type: Types.NEXT_PAGE
            })
        })

        it('should return correct action on setCurrentCharId', () => {
            const payload = 1002
            const action = setCurrentChatId(payload)
            expect(action).toStrictEqual({
                type: Types.SET_CURRENT_CHAR_ID,
                payload
            })
        })
    })

    describe('Reducers', () => {
        it('should return new state on action add dispatched', () => {
            const payload = {total: 1, items: [{id: 1, title: 'Serie Title'}]}
            const action = add(payload)
            const result = SeriesReducer({}, action)
            expect(result.total).toBe(1)
            expect(result.items).toStrictEqual({
                'item-1': {id: 1, title: 'Serie Title'}
            })
        })

        it('should return new state on action setLoading dispatched', () => {
            const payload = false
            const action = setLoading(payload)
            const result = SeriesReducer({}, action)
            expect(result.loading).toBe(false)
        })

        it('should return new state on action setTotal dispatched', () => {
            const payload = 1
            const action = setTotal(payload)
            const result = SeriesReducer({}, action)
            expect(result.total).toBe(1)
        })

        it('should return new state on action setHasMore dispatched', () => {
            const payload = false
            const action = setHasMore(payload)
            const result = SeriesReducer({}, action)
            expect(result.hasMore).toBe(false)
        })

        it('should return new state on action setError dispatched', () => {
            const payload = new Error('Error description')
            const action = setError(payload)
            const result = SeriesReducer({}, action)
            expect(result.error).toBe('Error description')
        })

        it('should return new state on action nextPage dispatched', () => {
            const action = nextPage()
            const result = SeriesReducer({pagination: {page: 1}}, action)
            expect(result.pagination.page).toBe(2)
        })

        it('should return new state on action setCurrentCharId dispatched', () => {
            const action = setCurrentChatId(1002)
            const result = SeriesReducer({}, action)
            expect(result.currentCharId).toBe(1002)
        })
    })
})