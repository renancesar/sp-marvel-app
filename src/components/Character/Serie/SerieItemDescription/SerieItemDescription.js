import React from 'react';
import './style.scss'

function SerieItemDescription({serie}) {
    return (
        <div className="serie-description" data-testid="serie-item-description">
            <div className="">
                {serie.description}
            </div>
        </div>
    )
}

export default SerieItemDescription;