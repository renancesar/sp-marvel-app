import React from 'react';
import './styles.scss'
import MarvelLogo from '../../../assets/images/marvel-logo.png'
import {Link} from "react-router-dom";
function Header(props) {
    return (
        <div className="app-header mb-4">

            <Link to="/" className="logo my-2" data-testid="header-home-link">
                <img src={MarvelLogo} alt="Marvel Logo" data-testid="header-marvel-logo"/>
            </Link>
        </div>
    );
}

export default Header;