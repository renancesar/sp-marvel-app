const mockAxios = jest.genMockFromModule('axios')

export default {
    get: jest.fn(() => Promise.resolve({ data: { results: [], total: 0} })),
    create: jest.fn(() => mockAxios)
}