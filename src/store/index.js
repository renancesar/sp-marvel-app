import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';

import createSagaMiddleware from 'redux-saga';

import { Reducers } from './ducks'
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();
export const makeStore = () => createStore(
    Reducers,
    composeWithDevTools(
        applyMiddleware(sagaMiddleware)
    )
)
const store = makeStore()

sagaMiddleware.run(rootSaga);

export default store
