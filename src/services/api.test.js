
import {cleanup} from "@testing-library/react";
import api, {generateAuthParams} from "./api";

describe('API Spec', () => {
    afterEach(cleanup)

    it('should generate params based on environment variables', () => {
        const DATE_TO_USE = new Date('2020');
        const _Date = Date;
        global.Date = jest.fn(() => DATE_TO_USE);
        global.Date.UTC = _Date.UTC;
        global.Date.parse = _Date.parse;
        global.Date.now = _Date.now;

        const {ts, apikey, hash} = generateAuthParams()

        expect(ts).toBe(+DATE_TO_USE)
        expect(apikey).toBe(process.env.REACT_APP_PUBLIC_KEY)
        expect(hash).toBe('7c95f9b85421f3c73453bf5abe9dc3b0')
    })

})