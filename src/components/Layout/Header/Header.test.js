import {render, cleanup, fireEvent} from '@testing-library/react';
import React from 'react';
import Header from './Header'
import {Router} from "react-router-dom";
import {createMemoryHistory} from 'history'

afterEach(cleanup)

describe('Header', () => {
    it('should render and compare snapshot', () => {
        const history = createMemoryHistory();
        const {asFragment} = render(<Router history={history}><Header/></Router>)
        expect(asFragment()).toMatchSnapshot();
    })

    it('Should render marvel logo with alt property defined', () => {
        const history = createMemoryHistory();
        const {getByTestId} = render(
            <Router history={history}>
                <Header />
            </Router>
        );
        const elem = getByTestId('header-marvel-logo')
        expect(elem.src.includes('marvel-logo.png')).toBeTruthy()
        expect(elem.alt === 'Marvel Logo').toBeTruthy()
    })

    it('should redirect to / route when click in logo', () => {
        const history = createMemoryHistory();
        const {getByTestId} = render(
            <Router history={history} initialEntries={['/character-details/1']}>
                <Header />
            </Router>
        );
        history.push('/character-details/1')
        const linkElement = getByTestId('header-home-link')

        expect(history.location.pathname).toBe("/character-details/1");``

        fireEvent.click(linkElement)

        expect(history.location.pathname).toBe(`/`);
    })
});