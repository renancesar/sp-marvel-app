import { call, select, put, all, takeLatest } from 'redux-saga/effects';
import {Types, setError, setLoading, add, setHasMore, setTotal} from "../ducks/SeriesDucks";
import SeriesService from "../../services/series";
import {defineHasMore} from '../../services/utils'

export function* getAllSeries() {
    try {
        yield put(setLoading(true))
        let {pagination, currentCharId, total} = yield select(state => state.Series)

        const response = yield call(SeriesService.getAllSeries, currentCharId, pagination)
        total = response.total
        yield put(setTotal(response.total))
        const hasMore = defineHasMore(pagination, total)
        yield put(setHasMore(hasMore))
        yield put(add(response))
        yield put(setLoading(false))
    } catch(err) {
        yield put(setError(err))
        yield put(setLoading(false))
    }
}

export default all([
    takeLatest(Types.FETCH_ALL, getAllSeries),
]);

