import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {get} from 'lodash'
import {findOne} from "../../store/ducks/CharacterDucks";
import {fetchSeries, setCurrentChatId, nextPage} from "../../store/ducks/SeriesDucks";
import './styles.scss'
import LoaderComponent from "../../components/Layout/Loader/LoaderComponent";
import SerieItem from "../../components/Character/Serie/SerieItem/SerieItemComponent";
import CharProfileEditable from "../../components/Character/Profile/CharProfileEditable";
import CharProfileReadOnly from "../../components/Character/Profile/CharProfileReadOnly";

function CharacterDetails({match}) {
    const [editMode, setEditMode] = useState(false)
    const dispatch  = useDispatch()
    const id = match.params.id
    let char = useSelector(store => get(store, 'Characters.current', {}))
    let series = useSelector(store => get(store, 'Series'))
    useEffect(() => {
        dispatch(findOne(id))
    })

    useEffect(() => {
        if (!char.id) return
        dispatch(setCurrentChatId(char.id))
        dispatch(fetchSeries())
    }, [char.id, dispatch])

    function viewMore () {
        dispatch(nextPage())
        dispatch(fetchSeries())
    }

    return (
        <div className="row">
            <div className="col-md-3">
                <div className="app-char-details d-flex align-content-center">
                    <div className="user-pic mr-3">
                        <div className="">
                            <img data-testid='character-detail-char-avatar' className="user-avatar img-thumbnail" src={`${get(char, 'thumbnail.path')}.${get(char, 'thumbnail.extension')}`} alt="Char profile pic" />
                        </div>
                    </div>
                    <button className="btn btn-link mt-2" onClick={(e) => setEditMode(true)}>Edit profile</button>
                    {editMode
                        ? <CharProfileEditable char={char} setEditMode={setEditMode}/>
                        : <CharProfileReadOnly char={char} />
                    }
                </div>
            </div>
            <div className="col-md-9">
                <section className="row series-section">
                    <div className="col-12 p-0 mb-1">
                        <h3>Series</h3>
                    </div>
                    { Object.keys(get(series, 'items', {}))
                        .map(id => <SerieItem key={id} serie={get(series, `items[${id}]`, {})}></SerieItem>)
                    }
                    {!series.loading && series.hasMore ? (
                        <div className="col-12 text-center mt-3">
                            <button className="btn btn-marvel rounded-pill" onClick={viewMore}>View More</button>
                        </div>
                    ) : null}

                    {series.loading ? <LoaderComponent /> : null}
                </section>
            </div>
        </div>
    );
}

export default CharacterDetails;