import React from 'react';
import {get} from  'lodash'
function CharProfileReadOnly({char}) {
    return (
        <div>
            <h3 className="pt-3">{get(char, 'name') }</h3>
            <p className="mt-4">{get(char, 'description')}</p>
        </div>
    );
}

export default CharProfileReadOnly;