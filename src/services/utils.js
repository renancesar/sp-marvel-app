import {isEmpty, get, omit} from 'lodash'

export const setItems = (stateItems, payload, omitFields = []) => {
    let items = {...stateItems}
    payload.map(item => {
        const existentEntity = get(items, `[item-${item.id}]`, item)
        items[`item-${item.id}`] = {...existentEntity, ...omit(item, omitFields)}
        return item
    })
    return items
}

export const mountPagination = (pagination) => {
    const {page, limit} = pagination

    return {
        limit,
        offset: (page -1) * limit
    }
}

export const mountFilterParams = (filter) => {
    const term = get(filter, 'term', '')
    if (isEmpty(term)) return
    return {nameStartsWith: term}
}

export const defineHasMore = (pagination, total) => {
    const {page, limit} = pagination
    const offset = page * limit
    return (total - offset) > 0
}