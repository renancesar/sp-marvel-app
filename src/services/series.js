import api from "./api";
import {get} from 'lodash'
import {mountPagination} from './utils'
const SeriesService = {
    async getAllSeries (id, pagination) {
        const paginationParams = mountPagination(pagination)
        const response = await api.get(`/characters/${id}/series`, {params: paginationParams})
        return {
            items: get(response, 'data.data.results', []),
            total: get(response, 'data.data.total', 0)
        }
    }
}
export default SeriesService