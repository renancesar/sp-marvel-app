import {render} from '../../../test-utils'
import React from "react";
import CharProfileReadOnly from './CharProfileReadOnly';
import {cleanup} from "@testing-library/react";

describe('CharProfileReadOnly Spec', () => {
    afterEach(cleanup)
    const char = {
        name: 'Hero name',
        description: 'Hero description text'
    }

    it('should render and compare with snapshot', () => {
        const { asFragment} = render(
            <CharProfileReadOnly char={char}></CharProfileReadOnly>
        )
        expect(asFragment()).toMatchSnapshot()
    })

})