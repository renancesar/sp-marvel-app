import {render} from '../../../../test-utils'
import React from "react";
import SerieItemComponent from './SerieItemComponent';
import {cleanup, fireEvent} from "@testing-library/react";

describe('Serie Item Component Spec', () => {
    afterEach(cleanup)
    const serieItem = {
        title: 'Serie Title',
        description: 'resume serie text'
    }

    it('should render and compare with snapshot', () => {
        const {asFragment} = render(<SerieItemComponent serie={serieItem}/>)
        expect(asFragment()).toMatchSnapshot()
    })

    it('should change icon when open serie description', () => {
        const {getByTestId} = render(<SerieItemComponent serie={serieItem}/>)
        let iconElement = getByTestId('serie-item-icon-view-more')
        expect(iconElement.firstChild).toHaveClass('fa-plus-square')
        fireEvent.click(iconElement)
        expect(iconElement.firstChild).toHaveClass('fa-minus-square')
    })

    it('should render serie description when is opened', () => {
        const {getByTestId} = render(<SerieItemComponent serie={serieItem}/>)
        let iconElement = getByTestId('serie-item-icon-view-more')
        fireEvent.click(iconElement)
        let serieDescriptionElement = getByTestId('serie-item-description')
        expect(serieDescriptionElement).toBeInTheDocument()
    })

    it('should not render serie description when is closed', () => {
        const {queryByTestId} = render(<SerieItemComponent serie={serieItem}/>)
        expect(queryByTestId(/serie-item-description/i)).toBeNull();
    })

    it('should not render icon when serie no has description', () => {
        let serieItemWithoutDescription = {...serieItem}
        delete serieItemWithoutDescription.description

        const {queryByTestId} = render(<SerieItemComponent serie={serieItemWithoutDescription}/>)
        expect(queryByTestId(/serie-item-icon-view-more/i)).toBeNull();
    })

})