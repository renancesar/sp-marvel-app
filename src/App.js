import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import './App.scss';
import Home from "./pages/Home/Home";
import CharacterDetails from "./pages/CharacterDetails/CharacterDetails";
import Header from "./components/Layout/Header/Header";
import Footer from "./components/Layout/Footer/Footer";

function App() {
    return (
        <div className="App">
                <BrowserRouter>
                    <Header />
                    <div className="main container" data-testid="app-main-container">
                        <Switch>
                            <Route path="/" exact={true} component={Home}/>
                            <Route path="/character-details/:id" component={CharacterDetails}/>
                        </Switch>
                    </div>
                    <Footer />
                </ BrowserRouter>
        </div>
    );
}

export default App;
