import React from 'react'
import { render as rtlRender } from '@testing-library/react'
import { Provider } from 'react-redux'
import ReduxStore from './store'
import {makeStore} from "./store";
const defaultStore = makeStore()
function render(ui, {store = defaultStore, ...renderOptions} = {}) {
    function Wrapper({ children }) {
        return <Provider store={store}>{children}</Provider>
    }
    return rtlRender(ui, { wrapper: Wrapper, ...renderOptions })
}

export function makeTestStore(opts = {}) {
    const store = makeStore()
    const origDispatch = store.dispatch
    store.dispatch = jest.fn(origDispatch)
    return store
}
// re-export everything
export * from '@testing-library/react'

// override render method
export { render }