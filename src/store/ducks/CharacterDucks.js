import {setItems} from '../../services/utils'
const OMITED_FIELDS_ON_UPDATE = ['name', 'description']
// Types
export const Types = {
    ADD: 'CHARACTERS/ADD',
    NEXT_PAGE: 'CHARACTERS/NEXT_PAGE',
    SET_FILTER: 'CHARACTERS/SET_FILTER',
    SET_LOADING: 'CHARACTERS/SET_LOADING',
    SET_CURRENT: 'CHARACTERS/SET_CURRENT',
    SET_ERROR: 'CHARACTERS/SET_ERROR',
    FIND_ONE: 'CHARACTERS/FIND_ONE',
    FETCH_ALL: 'CHARACTERS/FETCH_ALL',
    SET_HAS_MORE: 'CHARACTERS/SET_HAS_MORE',
    SET_TOTAL: 'CHARACTERS/SET_TOTAL',
    RESET: 'CHARACTERS/RESET',
    FETCH_SEARCH: 'CHARACTERS/FETCH_SEARCH',
    UPDATE_CHAR: 'CHARACTERS/UPDATE_CHAR',
    UPDATE_CHAR_PROFILE: 'CHARACTERS/UPDATE_CHAR_PROFILE'
}


// Reducer
const initialState = {
    characters: {},
    current: {},
    pagination: {page: 1, limit: 20},
    filter: {},
    error: {},
    loading: true,
    hasMore: true,
    total: 0
}

export default function (state = initialState, action) {
    switch (action.type) {
        case Types.ADD:
            return {
                ...state,
                characters: setItems(state.characters, action.payload.items, OMITED_FIELDS_ON_UPDATE)
            }

        case Types.NEXT_PAGE:
            return {
                ...state,
                pagination: {...state.pagination, page: state.pagination.page += 1}
            }

        case Types.SET_FILTER:
            return {
                ...state,
                filter: {...state.filter, ...action.payload}
            }

        case Types.SET_LOADING:
            return {
                ...state,
                loading: action.payload
            }

        case Types.SET_ERROR:
            return {
                ...state,
                error: action.payload
            }

        case Types.SET_CURRENT:
            return {
                ...state,
                current: action.payload
            }

        case Types.SET_HAS_MORE:
            return {
                ...state,
                hasMore: action.hasMore
            }

        case Types.SET_TOTAL:
            return {
                ...state,
                total: action.total
            }

        case Types.RESET:
            return {
                ...state,
                ...initialState
            }

        case Types.UPDATE_CHAR:
            return {
                ...state,
                characters: _updateCharById(state.characters, action.id, action.payload)
            }

        default:
            return state
    }
}

// Actions

export const findOne = id => ({
    type: Types.FIND_ONE,
    id
})

export const setCurrent = payload => ({
    type: Types.SET_CURRENT,
    payload
})

export const addCharacters = data => ({
    type: Types.ADD,
    payload: data
})

export const setLoading = value => ({
    type: Types.SET_LOADING,
    payload: value
})

export const nextPage = payload => ({
    type: Types.NEXT_PAGE
})

export const setFilter = filter => ({
    type: Types.SET_FILTER,
    payload: filter
})

export const reset = () => ({
    type: Types.RESET
})

export const setError = error => ({
    type: Types.SET_ERROR,
    payload: error.message
})

export const fetchCharacters = () => ({
    type: Types.FETCH_ALL
})

export const setHasMore = hasMore => ({
    type: Types.SET_HAS_MORE,
    hasMore
})

export const setTotal = total => ({
    type: Types.SET_TOTAL,
    total
})

export const updateChar = (id, payload) => ({
    type: Types.UPDATE_CHAR,
    id: id,
    payload
})

export const updateCharacterProfile = (id, payload) => ({
    type: Types.UPDATE_CHAR_PROFILE,
    id: id,
    payload
})

function _updateCharById (stateCharacters, id, payload) {
    const updateCharacters = {...stateCharacters}
    updateCharacters[`item-${id}`] = {
        ...updateCharacters[`item-${id}`],
        ...payload
    }
    return updateCharacters
}