import React from 'react';
import { render, cleanup } from '@testing-library/react';
import LoaderComponent from './LoaderComponent';

afterEach(cleanup)


test('renders loader component', () => {
    const { asFragment } = render(<LoaderComponent />);
    expect(asFragment()).toMatchSnapshot()
});
