import {render, makeTestStore} from '../../../test-utils'
import React from "react";
import SearchBarComponent from './SearchBarComponent';
import {cleanup, fireEvent} from "@testing-library/react";
import {fetchCharacters, reset, setFilter} from "../../../store/ducks/CharacterDucks";

describe('SearchBarComponent Spec', () => {
    afterEach(cleanup)

    it('should render and compare snapshot', () => {
        const {asFragment} = render(<SearchBarComponent />)
        expect(asFragment()).toMatchSnapshot()
    })

    it('should update input value on change', () => {
        const {getByPlaceholderText} = render(<SearchBarComponent />)
        const inputElement = getByPlaceholderText('Search here for the character you want')

        fireEvent.change(inputElement, {target:{value: 'Char name i want'}})

        expect(inputElement.value).toBe('Char name i want')
    })

    it('should update input value on change', () => {
        const store = makeTestStore()
        const {getByPlaceholderText, getByText} = render(<SearchBarComponent />, {store})
        const inputElement = getByPlaceholderText('Search here for the character you want')
        const buttonSearchElement = getByText(/search/i)
        fireEvent.change(inputElement, {target:{value: 'Char name i want'}})
        fireEvent.click(buttonSearchElement)
        const expectedSetFilterCall = setFilter({term: 'Char name i want'})
        expect(inputElement.value).toBe('Char name i want')
        expect(store.dispatch).toHaveBeenCalledTimes(3)
        expect(store.dispatch).toHaveBeenCalledWith(reset())
        expect(store.dispatch).toHaveBeenCalledWith(expectedSetFilterCall)
        expect(store.dispatch).toHaveBeenCalledWith(fetchCharacters())
    })

})