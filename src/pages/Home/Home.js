import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchCharacters, nextPage} from '../../store/ducks/CharacterDucks'
import SearchBarComponent from "../../components/Home/SearchBar/SearchBarComponent";
import './styles.scss'
import LoaderComponent from "../../components/Layout/Loader/LoaderComponent";
import CardComponent from "../../components/Home/Card/CardComponent";
function Home() {
    const dispatch  = useDispatch()
    let data = useSelector(store => store.Characters)
    useEffect(() => {
        dispatch(fetchCharacters())
    }, [dispatch])

    function viewMore () {
        dispatch(nextPage())
        dispatch(fetchCharacters())
    }

    return (
        <div>
            <SearchBarComponent />
            <div className="row">
                {Object.keys(data.characters).map((id) => (<CardComponent char={data.characters[id]}  key={id}/>))}

                {!data.loading && data.hasMore ? (
                    <div className="col-12 text-center mt-3">
                        <button className="btn btn-marvel rounded-pill" onClick={viewMore}>View More</button>
                    </div>
                ) : null}


                {data.loading ? <LoaderComponent /> : null}
            </div>
        </div>
    );
}

export default Home;