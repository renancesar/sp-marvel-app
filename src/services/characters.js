import api from "./api";
import {get, first} from 'lodash'
import {mountPagination, mountFilterParams} from "./utils";
const CharactersService = {
    async getAll(pagination, filter) {
        const paginationParams = mountPagination(pagination)
        const filterParams = mountFilterParams(filter)

        const params = {...paginationParams, ...filterParams}

        const response = await api.get('/characters', {params})
        return {
            items: get(response, 'data.data.results', []),
            total: get(response, 'data.data.total', 0)
        }
    },

    async findOne (id) {
        const response = await api.get(`/characters/${id}`)
        return first(get(response, 'data.data.results', []))
    }
}
export default CharactersService