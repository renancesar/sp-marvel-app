import axios from 'axios'
import CryptoJS from 'crypto-js'
const api = axios.create({
    baseURL: process.env.REACT_APP_API_URL
})

api.interceptors.request.use(async (config) => {
    const params = generateAuthParams()
    config.params = {...config.params, ...params}
    return config
}, error => {
    return Promise.reject(error)
})

export function generateAuthParams () {
    const apikey = process.env.REACT_APP_PUBLIC_KEY
    const privateKey = process.env.REACT_APP_PRIVATE_KEY
    const ts = +new Date()
    const hash = CryptoJS.MD5(`${ts}${privateKey}${apikey}`).toString()
    return {ts, apikey, hash}
}

export default api