import {cleanup, fireEvent} from '@testing-library/react';
import React from 'react';
import {render} from '../../../test-utils'
import {Router} from "react-router-dom";
import CardComponent from "./CardComponent";
import { createMemoryHistory } from "history"

describe('Card Component SPEC', () => {

    afterEach(cleanup)

    const fixtureChar = {
        id: 1,
        name: 'Test Hero',
        thumbnail: {
            path: 'http://route-to-image.com/image-id'  ,
            extension:'jpg'
        }
    }
    it('should render and show correct data in component', () => {
        const history = createMemoryHistory();
        const { getByTestId, asFragment, getByText } = render(
            <Router history={history}>
                <CardComponent char={fixtureChar}></CardComponent>
            </Router>
        )
        const charPictureElement = getByTestId('card-component-char-picture')
        const charName = getByText(fixtureChar.name)
        expect(charPictureElement.src.includes(`${fixtureChar.thumbnail.path}.${fixtureChar.thumbnail.extension}`)).toBeTruthy()
        expect(charName).toBeTruthy()

        expect(asFragment()).toMatchSnapshot()
    })

    it('should Link to char details page', () => {
        const history = createMemoryHistory();
        const { getByText } = render(
            <Router history={history} initialEntries={["/"]}>
                <CardComponent char={fixtureChar}></CardComponent>
            </Router>
        )
        expect(history.location.pathname).toBe("/");``

        fireEvent.click(getByText(/View Details/i))

        expect(history.location.pathname).toBe(`/character-details/${fixtureChar.id}`);
    })
})