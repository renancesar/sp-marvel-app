import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {updateCharacterProfile} from '../../../store/ducks/CharacterDucks'

function CharProfileEditable({char, setEditMode}) {
    const dispatch = useDispatch()
    const [name, setName] = useState(char.name)
    const [description, setDescription] = useState(char.description)

    function updateCharProfile () {
        dispatch(updateCharacterProfile(char.id, {name, description}))
        setEditMode(false)
    }
    return (
        <div>
            <div className="form-group w-100">
                <label>Name</label>
                <input data-testid="char-profile-editable-name-input" type="text" value={name} onChange={(e) => setName(e.target.value)} className="form-control"/>
            </div>
            <div className="form-group">
                <label>Description</label>
                <textarea
                    className="form-control"
                    data-testid="char-profile-editable-description-input"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    cols="30"
                    rows="10" />
            </div>
            <button className="btn btn-marvel" onClick={updateCharProfile}>Update</button>
        </div>
    );
}

export default CharProfileEditable;