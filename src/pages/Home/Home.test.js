import React from 'react';
import {render, cleanup, makeTestStore} from '../../test-utils';
import Home from './Home';
import {addCharacters, fetchCharacters, setCurrent, setLoading, setHasMore, nextPage} from "../../store/ducks/CharacterDucks";
import {Router} from "react-router";
import {createMemoryHistory} from 'history'
import {fireEvent} from "@testing-library/react";
let store
const chars = {
    total: 1,
    items: [
        {
            id: 1,
            name: 'Hero name',
            description: 'Hero Description'
        }
    ]
}
afterEach(cleanup)

beforeEach(() => {
    store = makeTestStore()
})

describe('Home SPEC', () => {
    it('should render and compare snapshot', () => {
        const {asFragment} = render(<Home />)
        expect(asFragment()).toMatchSnapshot()
    })

    it('should call fetch on render component', () => {
        render(<Home />, {store})
        expect(store.dispatch).toBeCalledWith(fetchCharacters())
    })

    it('should render char card when has characters in store', () => {
        const history = createMemoryHistory()
        const {queryByTestId, getByTestId} = render(
            <Router history={history}>
                <Home />
            </Router>
            , {store})
        expect(queryByTestId('card-component-char-picture')).toBeNull()
        store.dispatch(addCharacters(chars))
        expect(getByTestId('card-component-char-picture')).toBeInTheDocument()
    })

    it('should show Loader when is in loading', () => {
        const {getByTestId} =render(<Home />, {store})

        store.dispatch(setLoading(true))
        expect(getByTestId('app-loader')).toBeInTheDocument()
    })

    it('should hide Loader when is in loading', () => {
        const {queryByTestId} =render(<Home />, {store})

        store.dispatch(setLoading(false))
        expect(queryByTestId('app-loader')).toBeNull()
    })

    it('should hide View More when is in loading', () => {
        const {queryByText} =render(<Home />, {store})

        store.dispatch(setLoading(true))
        expect(queryByText('View More')).toBeNull()
    })

    it('should hide View More when is no has more', () => {
        const {queryByText} =render(<Home />, {store})
        store.dispatch(setLoading(false))
        store.dispatch(setHasMore(false))
        expect(queryByText('View More')).toBeNull()
    })

    it('should show View More when has more item and not is in loading', () => {
        const {queryByText} =render(<Home />, {store})
        store.dispatch(setLoading(false))
        store.dispatch(setHasMore(true))
        expect(queryByText('View More')).toBeInTheDocument()
    })

    it('should call dispatch with nextPage and fetchSeries when view more clicked', () => {
        const {getByText} =render(<Home />, {store})
        store.dispatch(setLoading(false))
        store.dispatch(setHasMore(true))
        const viewMoreElement = getByText('View More')
        expect(viewMoreElement).toBeInTheDocument()

        fireEvent.click(viewMoreElement)

        expect(store.dispatch).toHaveBeenCalledWith(nextPage())
        expect(store.dispatch).toHaveBeenCalledWith(fetchCharacters())
    })

})
