import React from 'react';
import './style.scss'

function LoaderComponent(props) {
    return (
        <div className="col-12 text-center" data-testid="app-loader">
            <div className="loader-component d-inline-block mt-4"></div>
        </div>
    );
}

export default LoaderComponent;