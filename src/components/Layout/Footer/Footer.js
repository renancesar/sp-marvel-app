import React from 'react';
import './styles.scss'
function Footer() {
    return (
        <div className="app-footer mt-4">
            <p className="m-0">
                &copy; Renan Cesar, 2020. Todos direitos reservados
            </p>
        </div>
    );
}

export default Footer;