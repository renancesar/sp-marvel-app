import React from 'react';
import './style.scss'
import {Link} from "react-router-dom";
import {get} from 'lodash'

function CardComponent({char}) {
    return (
        <>
            <div className="col-lg-2 col-md-4 col-sm-6 mt-4 d-flex align-items-stretch">
                <div className="card d-flex">
                    <div className="card-char-img">
                        <img
                            className="card-img-top"
                            data-testid="card-component-char-picture"
                            src={`${get(char, 'thumbnail.path')}.${get(char, 'thumbnail.extension')}`}
                            alt="Char profile pic" />
                    </div>
                    <div className="card-body card-char-body d-flex">
                        <h5 className="card-char-title">{char.name}</h5>

                        <Link className="card-char-view-details btn-marvel rounded-pill" to={`character-details/${char.id}`}>View Details</Link>
                    </div>
                </div>
            </div>
        </>
    )
}

export default CardComponent;