import {makeTestStore, render} from '../../../test-utils'
import React from "react";
import CharProfileEditable from './CharProfileEditable';
import {cleanup, fireEvent} from "@testing-library/react";
import {updateCharacterProfile} from "../../../store/ducks/CharacterDucks";


describe('CharProfileEditable Spec', () => {
    afterEach(cleanup)
    const char = {
        id: 1,
        name: 'Hero name',
        description: 'Hero description'
    }

    it('should render and compare with snapshot', () => {
        const {asFragment} = render(<CharProfileEditable char={char}/>)
        expect(asFragment()).toMatchSnapshot()
    })

    it('should dispatch update when save edits', () => {
        const store = makeTestStore()
        const setEditMode = jest.fn()
        const {asFragment, getByTestId, getByText} = render(<CharProfileEditable char={char} setEditMode={setEditMode}/>, {store})
        const updateButtonElement = getByText(/Update/i)
        fireEvent.click(updateButtonElement)
        expect(store.dispatch).toHaveBeenCalled()
        expect(setEditMode).toHaveBeenCalled()
    })

    it('should update name value on input change', () => {
        const store = makeTestStore()
        const setEditMode = jest.fn()
        const {getByTestId } = render(<CharProfileEditable char={char} setEditMode={setEditMode}/>, {store})
        const inputName = getByTestId('char-profile-editable-name-input')

        fireEvent.change(inputName, { target: { value: 'Hero changed name' } })

        expect(inputName.value).toBe('Hero changed name')
    })

    it('should update description value on input change', () => {
        const store = makeTestStore()
        const setEditMode = jest.fn()
        const {getByTestId } = render(<CharProfileEditable char={char} setEditMode={setEditMode}/>, {store})
        const inputDescription = getByTestId('char-profile-editable-description-input')

        fireEvent.change(inputDescription, { target: { value: 'Hero description changed' } })

        expect(inputDescription.value).toBe('Hero description changed')
    })

    it('should change values and dispatch update', () => {
        const store = makeTestStore()
        const setEditMode = jest.fn()
        const {getByTestId, getByText } = render(<CharProfileEditable char={char} setEditMode={setEditMode}/>, {store})
        const inputName = getByTestId('char-profile-editable-name-input')
        const inputDescription = getByTestId('char-profile-editable-description-input')
        const updateButtonElement = getByText(/Update/i)

        fireEvent.change(inputDescription, { target: { value: 'Hero description changed' } })
        fireEvent.change(inputName, { target: { value: 'Hero name changed' } })
        fireEvent.click(updateButtonElement)

        const expectedDistachCall = updateCharacterProfile(char.id, {name: 'Hero name changed', description: 'Hero description changed'})

        expect(store.dispatch).toHaveBeenCalledWith(expectedDistachCall)
        expect(setEditMode).toHaveBeenCalledWith(false)
    })

})