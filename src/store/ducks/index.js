import {combineReducers} from "redux";
import CharacterReducer from './CharacterDucks'
import SeriesReducer from './SeriesDucks'

export const Reducers = combineReducers({
    Characters: CharacterReducer,
    Series: SeriesReducer
})