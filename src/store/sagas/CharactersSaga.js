import { call, select, put, all, takeLatest } from 'redux-saga/effects';
import {
    Types,
    addCharacters,
    setLoading,
    setError,
    setCurrent,
    setHasMore,
    setTotal,
    updateChar
} from '../ducks/CharacterDucks'
import CharactersService from "../../services/characters";
import {defineHasMore} from "../../services/utils";

function* getAllCharacters() {
    try {
        yield put(setLoading(true))
        let {filter, pagination, total} = yield select(state => state.Characters)

        const response = yield call(CharactersService.getAll, pagination, filter)
        total = response.total
        yield put(setTotal(response.total))
        const hasMore = defineHasMore(pagination, total)
        yield put(setHasMore(hasMore))
        yield put(addCharacters(response))
        yield put(setLoading(false))
    } catch(err) {
        yield put(setError(err))
        yield put(setLoading(false))
    }
}

function* findOne({id}) {
    try {
        yield put(setLoading(true))
        const existentInState = yield select(state => state.Characters.characters[`item-${id}`])
        if (existentInState) {
            yield put(setCurrent(existentInState))
            yield put(setLoading(false))
            return
        }
        const charFromAPI = yield call(CharactersService.findOne, id)
        yield put(setCurrent(charFromAPI))
        yield put(addCharacters({items: [charFromAPI]}))
        yield put(setLoading(false))
    } catch(error) {
        yield put(setCurrent(null))
        yield put(setError(error))
        yield put(setLoading(false))
    }
}

function* updateCharacter ({id, payload}) {
    yield put(updateChar(id, payload))
    const updatedChar = yield select(state => state.Characters.characters[`item-${id}`])
    const current = yield select(state => state.Characters.current)

    yield put(setCurrent({...current, ...updatedChar}))
}

export default all([
    takeLatest(Types.FETCH_ALL, getAllCharacters),
    takeLatest(Types.FIND_ONE, findOne),
    takeLatest(Types.UPDATE_CHAR_PROFILE, updateCharacter)
]);