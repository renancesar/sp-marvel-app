import React, {useEffect, useState} from 'react';
import './style.scss'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlusSquare, faMinusSquare} from "@fortawesome/free-solid-svg-icons";
import SerieItemDescription from "../SerieItemDescription/SerieItemDescription";

function SerieItem({serie}) {
    const [opened, setOpened] = useState(false)
    const [viewMoreIcon, setViewMoreIcon] = useState(faPlusSquare)

    useEffect(() => {
        const icon = opened ? faMinusSquare : faPlusSquare
        setViewMoreIcon(icon)
    }, [opened])

    function toogle () {
        setOpened(!opened)
    }
    return (
        <div className="serie-item col-12 d-flex mt-3">
            <div className="serie-header d-flex">
                <h4 className="serie-title">{serie.title}</h4>
                {serie.description ? (
                    <div className="serie-view-more mr-2" onClick={toogle} data-testid="serie-item-icon-view-more">
                        <FontAwesomeIcon icon={viewMoreIcon}></FontAwesomeIcon>
                    </div>
                ) : null}

            </div>
            {opened ? <SerieItemDescription serie={serie} /> : null}
        </div>
    );
}

export default SerieItem;