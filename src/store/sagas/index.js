import { all } from 'redux-saga/effects';

import CharactersSaga from './CharactersSaga';
import SeriesSaga from './SeriesSaga';

export default function* rootSaga() {
    return yield all([CharactersSaga, SeriesSaga]);
}