import {setItems} from '../../services/utils'

// Types
export const Types = {
    ADD: 'SERIES/ADD',
    SET_LOADING: 'SERIES/SET_LOADING',
    SET_ERROR: 'SERIES/SET_ERROR',
    FETCH_ALL: 'SERIES/FETCH_ALL',
    NEXT_PAGE: 'SERIES/NEXT_PAGE',
    SET_CURRENT_CHAR_ID: 'SERIES/SET_CURRENT_CHAR_ID',
    SET_HAS_MORE: 'SERIES/SET_HAS_MORE',
    SET_TOTAL: 'SERIES/SET_TOTAL'

}
// Reducer
const initialState = {
    items: {},
    total: 0,
    pagination: {page: 1, limit: 20},
    error: {},
    currentCharId: null,
    loading: true,
    hasMore: true
}

export default function (state = initialState, action) {
    switch (action.type) {
        case Types.ADD:
            return {
                ...state,
                total: action.payload.total,
                items: setItems(state.items, action.payload.items)
            }

        case Types.NEXT_PAGE:
            return {
                ...state,
                pagination: {...state.pagination, page: state.pagination.page += 1}
            }

        case Types.SET_LOADING:
            return {
                ...state,
                loading: action.payload
            }

        case Types.SET_ERROR:
            return {
                ...state,
                error: action.payload
            }

        case Types.SET_CURRENT_CHAR_ID:
            return {
                ...state,
                currentCharId: action.payload
            }

        case Types.SET_HAS_MORE:
            return {
                ...state,
                hasMore: action.hasMore
            }

        case Types.SET_TOTAL:
            return {
                ...state,
                total: action.total
            }

        default:
            return state
    }
}
// Actions
export const fetchSeries = () => ({
    type: Types.FETCH_ALL
})

export const add = payload => ({
    type: Types.ADD,
    payload

})

export const nextPage = () => ({
    type: Types.NEXT_PAGE
})

export const setLoading = loading => ({
    type: Types.SET_LOADING,
    payload: loading
})

export const setError = error => ({
    type: Types.SET_ERROR,
    payload: error.message
})

export const setCurrentChatId = id => ({
    type: Types.SET_CURRENT_CHAR_ID,
    payload: id
})

export const setHasMore = hasMore => ({
    type: Types.SET_HAS_MORE,
    hasMore
})

export const setTotal = total => ({
    type: Types.SET_TOTAL,
    total
})